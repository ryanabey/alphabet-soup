package alphabet.soup;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class App {
    
    public static void main(String[] args) throws IOException {
        
        Grid grid = new Grid();

        /* Retreives the filename of the path */
        Path filename = Paths.get(args[0]);

        /* Retrieves the input file and displays the grid as a list of characters. */
        String inputFile = Files.readString(filename, StandardCharsets.UTF_8);

        InputFormat inputFormater = new InputFormat(inputFile);

        /* Represents the words to be found as an array of elements */
        ArrayList<String> wordsToBeFound = inputFormater.wordsToBeFound;

        /* Represents the grid of characters that have been parsed */
        char[][] gridOfChars = inputFormater.gridOfCharacters;

        /* Iterate through the words to be found to ensure that they are present in the grid */
        for (int index = 0; index < wordsToBeFound.size(); index++) {

            /* Performs depth-first search backtracking algorithm to find all the vertical, horizontal, 
            diagonal, forward, and backward characters until the word is found in the grid */
            System.out.print(grid.doesWordExist(gridOfChars, wordsToBeFound.get(index)) + "\n");
        }
    }
}

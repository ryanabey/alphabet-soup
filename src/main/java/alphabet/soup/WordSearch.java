package alphabet.soup;

public class WordSearch {
    
    /**
     * Recursive Depth First Search algorithm to start at the first position of the word and backtrack vertically, horizontally, diagonally, 
     * forwards, and backwards until the word is found.
     * 
     * Note: Depth-first search starts at the root node and explores as far as possible along each branch before backtracking.
     */

     public boolean depthFirstSearchWord(char[][] grid, int rows, int cols, String searchWord, int trackPosition) {

        /**
         * Base Cases to account for...
         */

        /**
         * If we finish traversing the entire word, we know we have found the word so we can return true.
         */
        if (trackPosition == searchWord.length()) {
            return true;
        }

        /**
         * What if we go out of bounds? If rows and columns are out of bounds return false.
         * 
         * 1. checks if rows is less than 0 (out of bounds)
         * 2. checks if cols is less than 0 (out of bounds)
         * 3. checks if rows is greater than the length of the grid (out of bounds)
         * 4. checks if columns is greater than length of the grid (out of bounds)
         * 5. If the character we are searching for in the index is != to the character we are at in our grid
         * 
         * Note: Since the grid of characters may appear vertical, horizontal, diagonal, forwards and backwards, we do not have to account for
         * visiting the same position twice within our path in the grid.
         */

        if (rows < 0 || cols < 0 || rows >= grid.length || cols >= grid[0].length || searchWord.charAt(trackPosition) != grid[rows][cols]) {
            return false;
        }

        /**
         * Run Depth-first search on all vertical, horizontal, diagonal, forward, and backward positions of the grid.
         * 
         * Note: trackPosition is being incremented by one because we found the character we are looking for in the grid and now are 
         * looking to find the next character in the grid.
         */

         
        /* Marks the current visited position in the grid */
        char tempPosition = grid[rows][cols];
        grid[rows][cols] = ' ';

        boolean traverseLeft = depthFirstSearchWord(grid, rows, cols - 1, searchWord, trackPosition + 1);
        boolean traverseRight = depthFirstSearchWord(grid, rows, cols + 1, searchWord, trackPosition + 1);

        boolean traverseUp = depthFirstSearchWord(grid, rows - 1, cols, searchWord, trackPosition + 1);
        boolean traverseDown = depthFirstSearchWord(grid, rows + 1, cols, searchWord, trackPosition + 1);

        boolean traverseUpLeft = depthFirstSearchWord(grid, rows - 1, cols - 1, searchWord, trackPosition + 1);
        boolean traverseUpRight = depthFirstSearchWord(grid, rows - 1, cols + 1, searchWord, trackPosition + 1);

        boolean traverseDownLeft = depthFirstSearchWord(grid, rows + 1, cols - 1, searchWord, trackPosition + 1);
        boolean traverseDownRight = depthFirstSearchWord(grid, rows + 1, cols + 1, searchWord, trackPosition + 1);

        /* This marks the current unvisited position in the grid */
        grid[rows][cols] = tempPosition;

        return (traverseLeft || traverseRight || traverseUp || traverseDown || traverseUpLeft || traverseUpRight || traverseDownLeft || traverseDownRight);
    }
}

package alphabet.soup;

import java.util.ArrayList;

public class InputFormat {

    public char[][] gridOfCharacters;
    public ArrayList<String> wordsToBeFound;

    private int[] rowsAndColsOfGrid;
    private char[][] grid;
    private ArrayList<String> tempWordsToBeFound;
    private String[] inputCharacters;
    private String delimiters;
    private final int lengthOfFirstLine;

    /* 
     * Constructor for InputFormat class to initialize objects to parse the given input files. 
     */
    public InputFormat(String str) { 
        /* 
         * Returns array of strings computed by splitting this string around matches of the given regular expression. 
         *
         */
        /* String split by newline using regex */
        this.delimiters = "\\n";
        this.inputCharacters = str.split(delimiters);
        this.lengthOfFirstLine = 2;
        this.tempWordsToBeFound = new ArrayList<>();
        this.rowsAndColsOfGrid = decomposeRowsAndColsOfGrid();
        this.gridOfCharacters = decomposeGridOfCharactersInWordSearch();
        this.wordsToBeFound = decomposeWordsToBeFound();
    }

    /**
     * Method to parse the first line which specifies the number of rows and columns in the grid of characters. This method determines how many rows
     * and columns of characters should be in the grid. 
     * 
     * Example: 
     * 
     * 10x10 --> once split 10 (row) is in position 0 and 10 (column) is in position 1. 
     */
    private int[] decomposeRowsAndColsOfGrid() {
        String firstLineOfInput = this.inputCharacters[0];

        String[] splitRowsAndCols = firstLineOfInput.split("x");

        //System.out.println(splitRowsAndCols); prints out String object

        /* Array to represent the length of the first line */
        int[] firstLineArray = new int[this.lengthOfFirstLine];

        /* Parses the rows of the first line which specifies the rows in the grid of characters */
        firstLineArray[this.lengthOfFirstLine - 2] = Integer.parseInt(splitRowsAndCols[this.lengthOfFirstLine - 2]);

        /* Parses the columns of the first line which specifies the rows in the grid of characters */
        firstLineArray[this.lengthOfFirstLine - 1] = Integer.parseInt(splitRowsAndCols[this.lengthOfFirstLine - 1]);

        // System.out.println(firstLineArray[0]);
        // System.out.println(firstLineArray[1]);

        return firstLineArray;
    }

    /* 
     * Function that provides the grid of characters in the word search and parses the input file into character bytes in memory. 
     */
    private char[][] decomposeGridOfCharactersInWordSearch() {
        /* Retrieves the first line that species the rows/cols of the grid of characters */
        String[] gridOfCharacters = this.inputCharacters;

        /* Retrieves the rows and columns of the first line that species the rows/cols of the grid as an object */
        int[] firstLineArray = this.rowsAndColsOfGrid;

        /* Convert the rows and columns of the first line that species the rows/cols of the grid into an Int value */
        int rows = firstLineArray[0], columns = firstLineArray[1], index = 1;

        this.grid = new char[rows][columns];

        /* Interate through the rows of the grid */
        while (index < (rows + 1)) {
            grid[index - 1] = gridOfCharacters[index].replaceAll(" ","").toCharArray();

            index++;
        }
        
        return grid;
    }

    /**
     * Function that specifies the words to be found. 
     */
    private ArrayList<String> decomposeWordsToBeFound() {
        /* Retrieves the first line that species the rows/cols of the grid of characters */
        String[] gridOfCharacters = this.inputCharacters;

        /* Retrieves the rows of the first line that species the rows of the grid as an object */
        int rowsOfGrid = this.rowsAndColsOfGrid[0], index = rowsOfGrid + 1;

        /* iterate through the grid of characters to analyze the words to be found. */
        while(index < gridOfCharacters.length) {
            tempWordsToBeFound.add(gridOfCharacters[index]);

            index++;
        }

        return tempWordsToBeFound;
    }
}

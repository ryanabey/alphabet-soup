package alphabet.soup;

public class Grid {

    /*
     * For Visualization Purposes
     *
     * 3x3
     * A B C
     * D E F
     * G H I
     * ABC
     * AEI
     */
    
    /**
     * This function checks to see if the word exists in the grid. If so it will return the word in the proper output format as shown in the README. 
     */
    
    public String doesWordExist(char[][] grid, String searchWord) {

        /* Calling WordSearch class */
        WordSearch wordSearch = new WordSearch();

        /*
         * To account for one of the conditions:
         * Words that have spaces in them will not include spaces when hidden in the
         * grid of characters.
         * Therefore, we will not include spaces for words.
         */
        searchWord = searchWord.replaceAll(" ", "");

        String emptyStr = "";

        /* Iterate through the rows of the grid */
        for (int rows = 0; rows < grid.length; rows++) {

            /* Iterate through the columns of the grid */

            for (int cols = 0; cols < grid[0].length; cols++) {

                int startPosition = rows, endPosition = cols;

                /* If the first letter of the word is found in the grid then continue */

                if (searchWord.charAt(0) == grid[rows][cols]) {

                    /**
                     * Run Depth-first search on all vertical, horizontal, diagonal, forwards, and
                     * backwards positions of the grid in every index of grid[rows][cols].
                     * 
                     * Note: trackPosition is being incremented by one because we found the
                     * character we are looking for in the grid.
                     */
                    
                    boolean traverseLeft = wordSearch.depthFirstSearchWord(grid, rows, cols - 1, searchWord, 1);
                    boolean traverseRight = wordSearch.depthFirstSearchWord(grid, rows, cols + 1, searchWord, 1);

                    boolean traverseUp = wordSearch.depthFirstSearchWord(grid, rows - 1, cols, searchWord, 1);
                    boolean traverseDown = wordSearch.depthFirstSearchWord(grid, rows + 1, cols, searchWord, 1);

                    boolean traverseUpLeft = wordSearch.depthFirstSearchWord(grid, rows - 1, cols - 1, searchWord, 1);
                    boolean traverseUpRight = wordSearch.depthFirstSearchWord(grid, rows - 1, cols + 1, searchWord, 1);

                    boolean traverseDownLeft = wordSearch.depthFirstSearchWord(grid, rows + 1, cols - 1, searchWord, 1);
                    boolean traverseDownRight = wordSearch.depthFirstSearchWord(grid, rows + 1, cols + 1, searchWord, 1);

                    if (traverseLeft) {
                        return searchWord + " " + startPosition + ":" + endPosition + " " + startPosition + ":"
                                + (endPosition - searchWord.length() + 1);
                    } 
                    if (traverseRight) {
                        return searchWord + " " + startPosition + ":" + endPosition + " " + startPosition + ":"
                                + (cols + searchWord.length() - 1);
                    } 
                    if (traverseUp) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition - searchWord.length() + 1) + ":" + endPosition;
                    } 
                    if (traverseDown) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition + searchWord.length() - 1) + ":" + endPosition;
                    } 
                    if (traverseUpLeft) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition - searchWord.length() + 1) + ":"
                                + (endPosition - searchWord.length() + 1);
                    } 
                    if (traverseUpRight) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition - searchWord.length() + 1) + ":"
                                + (endPosition + searchWord.length() - 1);
                    } if (traverseDownLeft) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition + searchWord.length() - 1) + ":"
                                + (endPosition - searchWord.length() + 1);
                    } 
                    if (traverseDownRight) {
                        return searchWord + " " + startPosition + ":" + endPosition + " "
                                + (startPosition + searchWord.length() - 1) + ":"
                                + (endPosition + searchWord.length() - 1);
                    }
                }
            }
        }

        return emptyStr;
    }
}

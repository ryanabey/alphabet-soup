package alphabet.soup;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest {
    /**
     * Unit Tests to check if the word exists in vertical, horizontal, diagonal, forward, and backward directions. 
     */
    
    private char[][] tempGrid = new char[][] {
        {'A', 'B', 'C'}, 
        {'D', 'E', 'F'}, 
        {'G', 'H', 'I'}
    };

    private Grid grid = new Grid();

    @Test
    public void searchWordLeftDirection() {
        assertEquals("FED 1:2 1:0", grid.doesWordExist(tempGrid, "FED"));
    }

    @Test
    public void searchWordRightDirection() {
        assertEquals("ABC 0:0 0:2", grid.doesWordExist(tempGrid, "ABC"));
    }

    @Test
    public void searchWordUpDirection() {
        assertEquals("HEB 2:1 0:1", grid.doesWordExist(tempGrid, "HEB"));
    }

    @Test
    public void searchWordDownDirection() {
        assertEquals("CFI 0:2 2:2", grid.doesWordExist(tempGrid, "CFI"));
    }

    @Test
    public void searchWordUpLeftDirection() {
        assertEquals("IEA 2:2 0:0", grid.doesWordExist(tempGrid, "IEA"));
    }

    @Test
    public void searchWordUpRightDirection() {
        assertEquals("GEC 2:0 0:2", grid.doesWordExist(tempGrid, "GEC"));
    }

    @Test
    public void searchWordDownLeftDirection() {
        assertEquals("CEG 0:2 2:0", grid.doesWordExist(tempGrid, "CEG"));
    }

    @Test
    public void searchWordDownRightDirection() {
        assertEquals("AEI 0:0 2:2", grid.doesWordExist(tempGrid, "AEI"));
    }
}
